#
# File: Makefile
# Creator: Jake Mitchell
# Created: Wed Sept 20 12:39:13 2017
#

CC = gcc
DIR = ./src/
IDIR = ./src/headers/
CFLAGS = -I$(IDIR) -g -std=c99 -Wall -Werror

PROGRAMS = auto

programs: $(PROGRAMS)

auto: $(DIR)main.c $(DIR)dfa.c $(DIR)dfa_builder.c $(DIR)dfa_tester.c
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	-rm $(PROGRAMS)
