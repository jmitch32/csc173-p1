### Project 1: Finite Automata ###
Jake Mitchell - jmitch32
20 September 2017

### Building And Running ###

All source files are located in the `src` folder.
All custom header files are within the `src/headers` folder.
All compiling is handled by the makefile.

To compile the source into an executable, simply run `make`.
To clear the compiled executable(s), run `make clean`.

To run the compiled program, run `./auto`.

### The Project

I didn't get to finish this project, but for the DFA section,
the builder file has functions to create the DFAs, the tester
file has all the functions to test each of the files and
allow for user testing, so you can test out my DFAs.
