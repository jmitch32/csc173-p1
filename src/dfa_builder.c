#include "dfa.h"
#include "dfa_builder.h"

DFA DFA_build_a() {
  DFA dfa = DFA_new(3, 2);
  char* chars[] = { "a", "b" };
  DFA_addCharacterMapping(dfa, chars);

  DFA_addState(dfa, (int[]){ 1, -1 });
  DFA_addState(dfa, (int[]){ -1, 2 });
  DFA_addState(dfa, (int[]){ -1, -1 });
  DFA_addAccepting(dfa, (int[]){ 2 }, 1);

  return dfa;
}

DFA DFA_build_b() {
  DFA dfa = DFA_new(3, 3);

  char allchars[128]; // create array for all characters
  for (int i = 1; i < 128; i++) {
    allchars[i - 1] = (char) i;
  }
  allchars[127] = '\0';

  char* chars[] = { "a", "b", allchars };
  DFA_addCharacterMapping(dfa, chars);

  DFA_addState(dfa, (int[]){ 1, -1, -1 });
  DFA_addState(dfa, (int[]){ -1, 2, -1 });
  DFA_addState(dfa, (int[]){ 2, 2, 2 });
  DFA_addAccepting(dfa, (int[]){ 2 }, 1);

  return dfa;
}

DFA DFA_build_c() {
  DFA dfa = DFA_new(3, 2);
  char* chars[] = { "0", "1" };
  DFA_addCharacterMapping(dfa, chars);

  DFA_addState(dfa, (int[]){ 0, 1 });
  DFA_addState(dfa, (int[]){ 1, 2 });
  DFA_addState(dfa, (int[]){ 2, 1 });
  DFA_addAccepting(dfa, (int[]){ 0, 2 }, 2);

  return dfa;
}

DFA DFA_build_d() {
  DFA dfa = DFA_new(9, 2);
  char* chars[] = { "0", "1" };
  DFA_addCharacterMapping(dfa, chars);

  DFA_addState(dfa, (int[]){ 3, 1 });
  DFA_addState(dfa, (int[]){ 5, 2 });
  DFA_addState(dfa, (int[]){ 6, 1 });
  DFA_addState(dfa, (int[]){ 4, 5 });
  DFA_addState(dfa, (int[]){ 3, 7 });
  DFA_addState(dfa, (int[]){ 7, 6 });
  DFA_addState(dfa, (int[]){ 8, 5 });
  DFA_addState(dfa, (int[]){ 5, 8 });
  DFA_addState(dfa, (int[]){ 6, 7 });
  DFA_addAccepting(dfa, (int[]){ 0, 2, 4, 8 }, 4);

  return dfa;
}
