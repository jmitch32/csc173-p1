#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "dfa.h"

DFA DFA_new(int num_states, int num_chars) {
  DFA dfa = (DFA) malloc(sizeof(struct _DFA));
  dfa->size = num_states;
  dfa->nChars = num_chars;
  dfa->filledStates = 0;
  dfa->states = malloc(num_states * sizeof(int *));
  dfa->mapping = malloc(num_chars * sizeof(char *));

  for (int i = 0; i < num_states; i++) {
    dfa->states[i] = malloc(num_chars * sizeof(int *));
  }
  for (int i = 0; i < num_chars; i++) {
    dfa->mapping[i] = malloc(128 * sizeof(char *));
  }

  return dfa;
}

void DFA_addCharacterMapping(DFA dfa, char* chars[]) {
  for (int i = 0; i < dfa->nChars; i++) { // for each character set column
    for (int j = 0; j < (int) strlen((char*) chars[i]); j++) { // for each character in the set
      dfa->mapping[i][j] = chars[i][j]; // add the character to the logged set
    }
  }
}

void DFA_addState(DFA dfa, int transitions[]) {
  for (int i = 0; i < dfa->nChars; i++) {
    dfa->states[dfa->filledStates][i] = transitions[i];
  }
  dfa->filledStates++;
}

void DFA_addAccepting(DFA dfa, int states[], int num_accepting) {
  dfa->accepting = malloc(num_accepting * sizeof(int *));
  dfa->nAccept = num_accepting;
  for(int i = 0; i < num_accepting; i++) {
    dfa->accepting[i] = states[i];
  }
}

int DFA_getTransition(DFA dfa, int curr_state, int char_col) {
  return dfa->states[curr_state][char_col];
}

int DFA_isAccepting(DFA dfa, int state) {
  for (int i = 0; i < dfa->nAccept; i++) {
    if (dfa->accepting[i] == state) {
      return 1;
    }
  }
  return 0;
}

int DFA_accepts(DFA dfa, char* str) {
  int current_state = 0;
  for (char* i = str; *i != '\0'; i++) { // iterate over string characters
    int col = -1;
    end_finding_char:
    if (col == -1) {
      for(int j = 0; j < dfa->nChars; j++) { // iterate over the map to get columns
        for (char* k = dfa->mapping[j]; *k != '\0'; k++) { // iterate over characters in column
          if (*k == *i) { // if character is found
            col = j; // use that column
            goto end_finding_char;
          }
        }
      }
    }
    if (col > -1) { // if we found a match for the character
      int state = DFA_getTransition(dfa, current_state, col);
      if (state > -1) { // if we find a state to transition into
        current_state = state;
      } else { // else it should fail
        return 0;
      }
    } else { // else it should fail
      return 0;
    }
  }
  return DFA_isAccepting(dfa, current_state);
}

void DFA_free(DFA dfa) {
  for (int i = 0; i < dfa->size; i++) {
    free(dfa->states[i]);
  }
  for (int i = 0; i < dfa->nChars; i++) {
    free(dfa->mapping[i]);
  }
  free(dfa->states);
  free(dfa->mapping);
  free(dfa->accepting);
  free(dfa);
}
