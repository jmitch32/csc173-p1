#ifndef DFA_H
#define DFA_H

typedef struct _DFA {
  int size;
  int nChars;
  int nAccept;
  int filledStates;
  int** states;
  char** mapping;
  int* accepting;
} *DFA;

extern DFA DFA_new(int num_states, int num_chars);

extern void DFA_addCharacterMapping(DFA dfa, char* chars[]);

extern void DFA_addState(DFA dfa, int transitions[]);

extern void DFA_addAccepting(DFA dfa, int states[], int num_accepting);

extern int DFA_getTransition(DFA dfa, int curr_state, int char_col);

extern int DFA_isAccepting(DFA dfa, int state);

extern int DFA_accepts(DFA dfa, char* str);

extern void DFA_free(DFA dfa);

#endif
