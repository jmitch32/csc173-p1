#ifndef DFA_BUILDER_H
#define DFA_BUILDER_H

#include "dfa.h"

// function to create and return each of the
// DFAs defined in part 1 of the project
extern DFA DFA_build_a();
extern DFA DFA_build_b();
extern DFA DFA_build_c();
extern DFA DFA_build_d();

#endif
