#ifndef DFA_TESTER_H
#define DFA_TESTER_H

#include "dfa.h"

// function to test each of the
// DFAs defined in part 1 of the project
extern void DFA_test_a();
extern void DFA_test_b();
extern void DFA_test_c();
extern void DFA_test_d();

extern void print_test_cases(DFA dfa, char* description, char** cases, int num_tests);
extern void ask_to_test_more(DFA dfa);

#endif
