#include <stdio.h>
#include "dfa.h"
#include "dfa_builder.h"
#include "dfa_tester.h"

void DFA_test_a() {
  DFA dfa_a = DFA_build_a();

  char* cases[] = { "a", "ab", "abc" };
  print_test_cases(dfa_a,
    "DFA a: accepts exactly \"ab\"", cases, 3);

  ask_to_test_more(dfa_a);
  DFA_free(dfa_a);
}

void DFA_test_b() {
  DFA dfa_b = DFA_build_b();

  char* cases[] = { "a", "ab", "abc", "abcd" };
  print_test_cases(dfa_b, "DFA b: accepts anything starting with \"ab\"", cases, 4);

  ask_to_test_more(dfa_b);
  DFA_free(dfa_b);
}

void DFA_test_c() {
  DFA dfa_c = DFA_build_c();

  char* cases[] = { "0", "01", "0101", "00110101", "0110100101" };
  print_test_cases(dfa_c, "DFA c: accepts binary with an even # of 1s", cases, 5);

  ask_to_test_more(dfa_c);
  DFA_free(dfa_c);
}

void DFA_test_d() {
  DFA dfa_d = DFA_build_d();

  char* cases[] = { "0", "01", "0101", "00110101", "0110100101" };
  print_test_cases(dfa_d, "DFA d: accepts binary with an even # of 1s AND 0s", cases, 5);

  ask_to_test_more(dfa_d);
  DFA_free(dfa_d);
}

void print_test_cases(DFA dfa, char* description, char** cases, int num_tests) {
  printf("%s\n", description);
  for (int i = 0; i < num_tests; i++) {
    printf(" - testing \"%s\": %d\n", cases[i], DFA_accepts(dfa, cases[i]));
  }
}

void ask_to_test_more(DFA dfa) {
  char str[BUFSIZ];

  printf("Would you like to test this DFA more (y/n)? ");
  scanf("%s", str);

  if (str[0] == 'y') {
    printf("At any point, type \"q\" to quit testing.\n");
    while (str[0] != 'q') {
      printf("Please enter a string to test: ");
      scanf("%s", str);
      if(str[0] == 'q') {
        break;
      }
      printf(" - testing \"%s\": %d\n", str, DFA_accepts(dfa, str));
    }
  }
  printf("\n");
}
